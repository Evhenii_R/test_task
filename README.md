# Excersize project
---
## Currency rate history

### Description

    This is implement web-application that allows you to save currency rates and display chart based on this data.

### Technologies Stack
-  Back-end: Python 3, Ajax, ORM: SQLAlchemy.

-  DBMS: MySQL.

-  Frameworks: Flask.

-  Front-end: Bootstrap, Ajax.

### Installation, Configuration, and Startup instructions
- To run this application, the user must have installed **Python 3**, **MySQL(localhost: 3306)**. Login and password to the database must be entered into **environment variables**(named as TEST_DB_LOGIN and TEST_DB_PASSWORD). 

- All requirements stored in **requirements.txt** and could be installed by command **pip install -r requirements.txt**.

- Database initialization required to get started. The database must be created from the attached file **setup.sql**. The first time the server should be started using the terminal with the **--initialize**(python runserver.py --initialize) option to create database tables.

- Other libraries (preferably Front-end) are connected to the HTML-file using the CDN (**bootstrap**, **jQuery**, **Chart.js**, **Ajax**)

### Contact Information
    Evhenii Reshetnyak
    mobile/Viber/Telegram: +380994372757
    jivtone@gmail.com
    Kharkiv, Ukraine

