from myapp import app, orm


if __name__ == '__main__':
    initializing_database = orm.db_initialize()

    if initializing_database.initialize:
        orm.Base.metadata.create_all(orm.engine)
        print('Database initialized!')

    app.run(debug=True)