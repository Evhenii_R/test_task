from flask import render_template, request, jsonify
from myapp import config
from myapp import util, orm
import sqlalchemy.exc
from myapp import app


@app.route('/', methods=['GET'])
def get_data():
    # Render main page.
    return render_template('index.html') 

 
#  Endpoint which sends data from database to front-end.
@app.route('/get_data', methods=['GET'])
def data_from_db():
    data_result = orm.fetch_data_from_db()
    return jsonify({'data' : data_result})


#  Endpoint which receives form data and verify it.
@app.route('/data', methods=['POST'])
def set_data():
    data_from_ajax = request.get_json()                                   
    form_date = data_from_ajax['date_from_form']
    form_rate = data_from_ajax['rate_from_form']

    #  Validate entered rate in to form.
    if len(str(form_rate)) == 0:
    #  Sends errors text to front end.
        return jsonify({'error' : '*Enter the currency rate, please!'})
    elif not util.is_float(form_rate):        
        return jsonify({'error' : '*Enter the correct rate, please!'})
    elif float(form_rate) <= 0:   
        return jsonify({'error' : '*Enter a positive rate, please!'})
    else:
        try:
            #  Insert data from form to a database.
            info_from_user = orm.RateHistory(curr_date=str(form_date), curr_rate=str(form_rate))
            orm.session.merge(info_from_user)  #  db.session.merge() method which update primary key on duplicate.
            orm.session.commit()
            data_result = orm.fetch_data_from_db()
        except sqlalchemy.exc.DBAPIError as err:
            orm.session.rollback()
            return jsonify({'error' : err})
    return jsonify({'data' : data_result})




