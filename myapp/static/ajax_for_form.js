$(document).ready(function() {
    function getData(data) {
            if (data.hasOwnProperty('error')) {
                $('#error_alert').text(data.error).show();
            }
            else {
                $('#error_alert').hide();
                dates = [];
                rates = [];
                var theActualData = data["data"];
                for (var i = 0; i < theActualData.length; i++) {
                    dates.push(new Date(theActualData[i][0]));
                    rates.push(theActualData[i][1]);
                };              
                dates = dates.map(function(d) { return d.getDate() + '/' + (d.getMonth() + 1) + '/' + d.getFullYear() });

                if (dates.length == 0) {
                    $('#hidden-div').addClass('hidden');
                } else {
                    $('#hidden-div').removeClass('hidden');
                    var ctx = $('#myChart');
                    var myChart = new Chart(ctx, {
                        type: 'line',
                        data: {
                            labels: dates,
                            datasets: [{
                                label: 'Currency rate',
                                data: rates,
                                backgroundColor: [
                                    'rgba(255, 99, 132, 0.2)'
                                ],
                                borderColor: [
                                    'rgba(255, 99, 132, 1)'                                  
                                ],
                            borderWidth: 1
                        }]
                    },
                    options: {
                        responsive: true,
                        scales: {
                            yAxes: [{
                                ticks: {
                                    beginAtZero: true
                                }
                            }]
                        }
                    }
                    });
                    }        
                    }
                };

    $.get('/get_data')
    .done(getData);

    $('#send_data').click(function(event) {
        event.preventDefault();
        data_from_form = {  
            rate_from_form : $('#rate_input').val(),
            date_from_form : $('#date_input').val()
        };
        $.ajax({
            type : 'POST',
            url : '/data',
            data : JSON.stringify(data_from_form),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json'
        })        
        .done(function(data) {
            getData(data);
            $("#form1")[0].reset();
        });       
        return false;
    });
});