#  Function for validation rate value.
def is_float(val):
    try:
        val = float(val)
        return True
    except ValueError:
        return False