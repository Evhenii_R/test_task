import os


#  Login and Password from the Database stored in virtual environment.
TEST_DB_LOGIN = os.getenv('TEST_DB_LOGIN')
TEST_DB_PASSWORD = os.getenv('TEST_DB_PASSWORD')


if TEST_DB_LOGIN == None:
    print('Set TEST_DB_LOGIN into Environment Variables!')
    exit(1)
elif TEST_DB_PASSWORD == None:
    print('Set TEST_DB_PASSWORD into Environment Variables!')
    exit(1)
