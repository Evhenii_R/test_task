from myapp import config
import argparse
from sqlalchemy import create_engine, Table, Column, Date, Integer
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

#  Func, which create SQLAlchemy connection to my database.
def do_database_connection(config):
    try:
        engine = create_engine(f'mysql+mysqlconnector://{config.TEST_DB_LOGIN}:{config.TEST_DB_PASSWORD}@localhost/rate_db', echo=False)
        return engine
    except:
        print('Wrong login or password from database!')
        exit(1)

#  Create SQLAlchemy's connection and session to my database.
engine = do_database_connection(config)
Session = sessionmaker(bind=engine)
session = Session()

Base = declarative_base()


#  Description of the base structure.
class RateHistory(Base):
    __tablename__ = 'rate_history'

    curr_date = Column(Date, nullable=False, primary_key=True)
    curr_rate = Column(Integer, nullable=False)

    #  Method, serializing orm's objects to dict.
    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}

    def __repr__(self):
        return "<RateHistory(curr_date='%s', curr_rate='%s')>" % (
                                    self.curr_date, self.curr_rate)


#  Function for initialize database via argparse module.
def db_initialize():
    parser = argparse.ArgumentParser(description='Argument for database initializtion.')
    parser.add_argument(
        '--initialize',
        dest='initialize',
        action='store_true',
        help='Call, if you need to itialize database.'
    )
    return parser.parse_args()


#  Get data from database.
def fetch_data_from_db():
    data_result = []
    db_query = session.query(RateHistory).all()
    data_temp = [elem.as_dict() for elem in db_query]
    for elem in data_temp:
        data_result.append([elem.get('curr_date'), elem.get('curr_rate')])   
    return data_result
